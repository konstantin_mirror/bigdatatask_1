package com.bigdata;

import com.bigdata.mappers.TokenizerMapper;
import com.bigdata.reducers.TheLongestWordReducer;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;


/**
 * Created with IntelliJ IDEA.
 * User: pascal
 */
public class TheLonegestWordFinderJobTest
{

    private MapDriver<Object, Text, IntWritable, Text> mapDriver;
    private ReduceDriver<IntWritable, Text, IntWritable, Text> maxReduceDriver;

    @Before
    public void setUp()
    {
        Mapper mapper = new TokenizerMapper();
        Reducer reducer = new TheLongestWordReducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        maxReduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    /**
     * Test for map task
     * It should return pairs word:number.
     *
     * @throws IOException
     */
    @Test
    public void testMapper() throws IOException
    {
        mapDriver.withInput(new Text(""), new Text("TEST_1 TEST_2"));
        mapDriver.withOutput(new IntWritable(6), new Text("TEST_1"));
        mapDriver.withOutput(new IntWritable(6), new Text("TEST_2"));
        mapDriver.runTest();
    }

    /**
     * Test for reduce task
     * It should return a word with max occurrences.
     *
     * @throws IOException
     */
    @Test
    public void testMaxReducer() throws IOException
    {
        maxReduceDriver.withInput(new IntWritable(6), Collections.singletonList(new Text("TEST_1")));
        maxReduceDriver.withOutput(new IntWritable(6),new Text("TEST_1"));
        maxReduceDriver.runTest();
    }

}