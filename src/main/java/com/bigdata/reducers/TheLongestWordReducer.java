package com.bigdata.reducers;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class TheLongestWordReducer
        extends Reducer<IntWritable, Text, IntWritable, Text>
{

    private int wordLongs = 0;

    @Override
    public void reduce(IntWritable key, Iterable<Text> values,
                       Context context) throws IOException, InterruptedException
    {
        if (wordLongs < key.get()){
            Set<String> logestWords = new HashSet<>();
            wordLongs = key.get();
            logestWords.clear();
            for (Text val : values)
            {
                logestWords.add(val.toString());
            }
            context.write(key, new Text(logestWords.toString()));
        }
    }
}
