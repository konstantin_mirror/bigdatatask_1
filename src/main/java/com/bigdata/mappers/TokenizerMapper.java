package com.bigdata.mappers;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;

public class TokenizerMapper
        extends Mapper<Object, Text, IntWritable, Text>
{

    @Override
    public void map(Object key, Text value, Context context
    ) throws IOException, InterruptedException
    {
        StringTokenizer itr = new StringTokenizer(value.toString());
        while (itr.hasMoreTokens())
        {
            String currentWord = itr.nextToken();
            context.write(new IntWritable(currentWord.length()), new Text(currentWord));
        }
    }
}