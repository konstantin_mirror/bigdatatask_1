package com.bigdata;

import com.bigdata.comparators.SortIntComparator;
import com.bigdata.mappers.TokenizerMapper;
import com.bigdata.reducers.TheLongestWordReducer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class TheLongestWordFinderJob
{

    public static void main(String[] args) throws Exception
    {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "Max word count");
        job.setJarByClass(TheLongestWordFinderJob.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(TheLongestWordReducer.class);
        job.setReducerClass(TheLongestWordReducer.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(Text.class);
        job.setSortComparatorClass(SortIntComparator.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
