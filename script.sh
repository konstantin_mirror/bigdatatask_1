#!/usr/bin/env bash

OUTPUT_FOLDER="/user/hduser/out"
INPUT_FOLDER="/user/hduser/in"
REPO="https://konstantin_mirror@bitbucket.org/konstantin_mirror/bigdatatask_1.git"

echo "Start script.."
git pull
echo  OUTPUT is ${OUTPUT_FOLDER}
echo  INPUT is ${INPUT_FOLDER}
echo  REPO is ${REPO}

if $(hadoop fs -test -d /user/hduser/out/) ;
 then
 echo "We should remove existen output folder"
 hadoop fs -rm -r ${OUTPUT_FOLDER}
else echo "Out folder is empty";
fi

echo "put files to hdfs"
hadoop fs -copyFromLocal -f  Input/*.txt ${INPUT_FOLDER}

sh gradlew jar

hadoop jar build/libs/task_1-1.0.jar  ${INPUT_FOLDER} ${OUTPUT_FOLDER}